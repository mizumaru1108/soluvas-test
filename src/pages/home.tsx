import { Avatar, Button, Typography } from '@mui/material';
import { Box } from '@mui/system';
import ResponsiveAppBar from '../components/navbar';
import Paper from '@mui/material/Paper';
import Footer from '../components/footer';
import ElectricBoltRoundedIcon from '@mui/icons-material/ElectricBoltRounded';
import AddIcon from '@mui/icons-material/Add';

interface ISpeciality {
  specialityTitle: string;
  specialityDesc: string;
  specialityImageUrl: string;
}

// const Item = styled(Paper)(({ theme }) => ({
//   ...theme.typography.body2,
//   // textAlign: 'center',
//   backgroundColor: 'gray',
//   color: 'white',
//   height: 60,
//   lineHeight: '60px',
//   customBorderRadius: {
//     borderRadius: 25,
//   },
// }));

const Home = () => {
  const tmraSpecalities: ISpeciality[] = [
    {
      specialityTitle: 'Focus on impact not fundraising',
      specialityDesc:
        'Tmra empowers donors to support the causes that will have the most impact by providing detailed impact reporting for each campaign.',
      specialityImageUrl:
        'https://staging.tmra.io/static/icons/landing/home/5929214_avatar_doctor_health_hospital_man_icon.svg',
    },
    {
      specialityTitle: 'Think charity programs not fundraising projects',
      specialityDesc:
        'At Tmra, we believe in the simple idea that everyone in the world can benefit from our technology. We offer a platform designed to optimize the donation process with the highest level of transparency possible. Our platform is revolutionary because it receives donations with minimal project costs to charities. We put trust back in charity.',
      specialityImageUrl:
        'https://staging.tmra.io/static/icons/landing/home/7709125_donation_charity_donate_islam_icon.svg',
    },
    {
      specialityTitle: 'Sustain talents and ongoing operations',
      specialityDesc:
        "Tmra is a nonprofit SaaS platform for volunteer sustainment and long term operations support. Tmra's cloud-based app helps organizations to recruit, train, manage, and engage volunteers, sustain volunteer impact, and to drive long term operations.",
      specialityImageUrl:
        'https://staging.tmra.io/static/icons/landing/home/4698588_call_communication_help_service_support_icon.svg',
    },
  ];

  return (
    <>
      <ResponsiveAppBar />
      <Box
        sx={{
          width: '100%',
          height: '100%',
        }}
      >
        <section>
          <Box
            sx={{
              height: '100vh',
              display: 'flex',
              flexDirection: 'row',
            }}
          >
            <Box
              sx={{
                backgroundImage:
                  "url('https://staging.tmra.io/static/home/young-pretty-muslim-business-woman-in-white-hijab-medium.jpg')",
                backgroundColor: 'black',
                backgroundRepeat: 'no-repeat',
                backgroundAttachment: 'fixed',
                backgroundSize: 'cover',
                height: '100%',
                width: '100%',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                }}
              >
                <Box
                  width={580}
                  sx={{
                    widht: { xs: '40%', sm: '30%', md: '40%' },
                    padding: { xs: 3, sm: 4, md: 1 },
                  }}
                >
                  <Box
                    sx={{
                      background: 'none',
                      justifyContent: 'left',
                      paddingLeft: 10,
                      display: 'absolute',
                    }}
                  >
                    <Typography
                      variant={'h3'}
                      color="white"
                      align="left"
                      pt={8}
                      sx={{
                        fontFamily: 'Almarai,sans-serif',
                        fontWeight: 'bold',
                        fontSize: { xs: '3vh', sm: '7vh', md: '8.8vh' },
                        lineHeight: 1.4,
                        textShadow: '#000000 0 0 1rem',
                        marginTop: 2,
                      }}
                    >
                      A donation-based crowdfunding platform for muslims
                    </Typography>
                    <Box
                      sx={{
                        marginTop: 5,
                        fontWeight: 'bold',
                        fontSize: '0.9vh',
                        paddingBottom: 0,
                      }}
                    ></Box>
                    <Typography
                      color="white"
                      variant={'body1'}
                      align="left"
                      sx={{
                        fontWeight: 400,
                        fontSize: '1rem',
                        textShadow: '#000000 0 0 1.5rem',
                      }}
                    >
                      Give your donors a super engaging experience that makes
                      them want to make monthly donations.
                    </Typography>
                    <Box
                      sx={{
                        marginTop: 5,
                        paddinTop: 0,
                      }}
                    >
                      <Button
                        variant="contained"
                        href="#"
                        startIcon={<ElectricBoltRoundedIcon />}
                        sx={{
                          color: 'white',
                          backgroundColor: '#00AB55',
                          height: '7vh',
                          width: '21vh',
                          textTransform: 'capitalize',
                          fontWeight: 'bold',
                          fontSize: '2.2vh',
                        }}
                      >
                        Start Free
                      </Button>
                    </Box>
                  </Box>
                </Box>
                <Box
                  sx={{
                    height: '100vh',
                  }}
                ></Box>
              </Box>
              <Button
                variant="contained"
                href="https://mui.com/material-ui/material-icons/?query=plus"
                startIcon={<AddIcon />}
                sx={{
                  position: 'fixed',
                  right: { xs: 10, sm: 0, md: 0 },
                  top: 110,
                  borderTopLeftRadius: 18,
                  borderBottomLeftRadius: 18,
                  width: { xs: '25vh', sm: '25vh', md: '23vh' },
                  fontWeight: 'bold',
                  textTransform: 'capitalize',
                  fontSize: { xs: '1.8vh', sm: '1.9vh', md: '1.9vh' },
                  backgroundColor: '#1890ff',
                }}
              >
                Quick Donate
              </Button>
            </Box>
          </Box>
        </section>
        <section>
          <Box
            sx={{
              width: '100%',
              height: '100%',
              backgroundColor: 'white',
              paddingBottom: 4,
              display: 'relative',
              zIndex: 2,
            }}
          >
            <Box>
              <Typography
                variant="h3"
                sx={{
                  fontWeight: 'bold',
                  lineHeight: 1.3,
                  fontFamily: 'Almarai, sans-serif',
                  textTransform: 'capitalize',
                  textAlign: 'center',
                  marginBottom: 13,
                  color: '##212b36',
                  fontSize: { md: '7.3vh', sm: '7.3vh', xs: '5vh' },
                  paddingTop: 13,
                }}
              >
                Monthly donations are transformative.
              </Typography>
            </Box>

            <Box
              sx={{
                padding: 5,
                display: 'flex',
                flexDirection: { xs: 'column', sm: 'column', md: 'row' },
              }}
            >
              {tmraSpecalities.map((speciality) => (
                // {speciality.specialityTitle}
                <Paper
                  variant="outlined"
                  sx={{
                    padding: 3,
                    width: { xs: '25vh', sm: '80vh', md: '50vh' },
                    margin: 5,
                    background: '#F4F6F8',
                    borderRadius: 5,
                    border: 0,
                  }}
                >
                  <Box sx={{ width: '100%' }}>
                    <Avatar
                      variant="square"
                      src={speciality.specialityImageUrl}
                      sx={{
                        width: 100,
                        height: 100,
                        margin: 'auto',
                        paddingBottom: 4,
                      }}
                    />
                    <Typography
                      sx={{
                        textAlign: 'center',
                        fontFamily: 'Almarai, sans-serif',
                        textTransform: 'capitalize',
                        fontWeight: 'bold',
                        fontSize: '2.4vh',
                        width: '100%',
                        padding: 0,
                        margin: 0,
                        color: '##212b36',
                      }}
                    >
                      {speciality.specialityTitle}
                    </Typography>
                    <Typography
                      sx={{
                        textAlign: 'center',
                        fontFamily: 'Almarai, sans-serif',
                        textTransform: 'capitalize',
                        fontSize: '2vh',
                        paddingTop: 3,
                        color: '##212b36',
                      }}
                    >
                      {speciality.specialityDesc}
                    </Typography>
                  </Box>
                </Paper>
              ))}
            </Box>
          </Box>
        </section>
        <section>
          <Footer />
        </section>
      </Box>
    </>
  );
};
export default Home;
