import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import MenuItem from '@mui/material/MenuItem';
import { makeStyles } from '@mui/styles';
import {
  createStyles,
  Divider,
  Drawer,
  Link,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  styled,
  Theme,
} from '@mui/material';

const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

const drawerWidth = 200;

const useStyles: any = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      align: 'right',
    },
    root1: {
      '&hover': {
        backgroundColor: 'transparent',
      },
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      flexShrink: 0,
      width: drawerWidth,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('md')]: {
        display: 'realative',
      },
    },
    toolbar: {
      ...theme.mixins.toolbar,
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      padding: theme.spacing(3),
    },
  }),
);

const ResponsiveAppBar = () => {
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
    null,
  );
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null,
  );

  const classes = useStyles;

  // const isMdUp = useMediaQuery(theme.breakpoints.up("md"));

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const StyledMenuBar = styled(Box)({
    display: 'realative',
    justifyContent: 'right',
  });
  const StyledToolbar = styled(Toolbar)({
    display: 'flex',
    flexDirection: 'row',
  });

  const MenuAppBar = [
    { Name: 'How It Works', Link: '#' },
    { Name: 'Pricing', Link: '#' },
    { Name: 'Sign In', Link: '#' },
    { Name: 'Start Free', Link: '#' },
  ];
  return (
    <AppBar
      sx={{
        background: 'none',
        boxShadow: 'none',
        display: 'absolute',
        top: 0,
      }}
    >
      <Container maxWidth="xl">
        <StyledToolbar
          disableGutters
          sx={{ margin: 3, marginLeft: { xs: 4, md: 9 } }}
        >
          <Box sx={{ cursor: 'pointer', height: 40, width: 120 }}>
            {/* <AdbIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} /> */}
            <a href="#">
              <img
                src="https://staging.tmra.io/static/tmra-brand-light-en.svg"
                alt="a"
              />
            </a>
          </Box>

          <Box
            sx={{
              flexGrow: 1,
              display: { xs: 'flex', md: 'none', justifyContent: 'right' },
            }}
          >
            <Drawer
              className={classes.drawer}
              variant={'temporary'}
              // variant={isMdUp ? "permanent" : "temporary"}
              classes={{
                paper: classes.drawerPaper,
              }}
              anchor="left"
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
            >
              <div className={classes.toolbar} />
              <Divider />
              <List>
                {['How It Work', 'Pricing', 'Sign In', 'StartFree'].map(
                  (text) => (
                    <ListItem button key={text}>
                      <ListItemIcon>
                        {/* {index % 2 === 0 ? <InboxIcon /> : <MailIcon />} */}
                      </ListItemIcon>
                      <ListItemText primary={text} />
                    </ListItem>
                  ),
                )}
              </List>
              <Divider />
            </Drawer>
          </Box>

          <StyledMenuBar
            sx={{
              flexGrow: 1,
              display: {
                xs: 'none',
                md: 'flex',
              },
            }}
          >
            {MenuAppBar.map((item) => (
              <Link underline="none" href={item.Link}>
                <Typography
                  sx={{
                    textDecoration: 'none',
                    color: 'white',
                    fontFamily: 'Tajawal, sans-serif, font',
                    margin: '0px 40px 0px 0px',
                    textTransform: 'capitalize',
                    fontWeight: 'bold',
                  }}
                >
                  {item.Name}
                </Typography>
              </Link>
            ))}
            {/* {pages.map((page) => (
              <Button
                key={page}
                onClick={handleCloseNavMenu}
                sx={{
                  my: 2,
                  color: 'white',
                  display: 'block',
                  fontWeight: 600,
                  lineHeight: 1.5,
                  fontSize: '0.9rem',
                  fontFamily: 'Almarai, sans-serif',
                  textTransform: 'capitalize',
                }}
              >
                {page}
              </Button>
            ))} */}
          </StyledMenuBar>

          <Box sx={{ flexGrow: 0, paddingRight: 1 }}>
            <Box sx={{ display: 'flex', flexDirection: 'row' }}>
              <Box
                sx={{ cursor: 'pointer', height: 30, width: 35, padding: 1 }}
              >
                <ShoppingCartIcon sx={{ fontSize: 28 }} />
              </Box>
              <Box
                sx={{
                  cursor: 'pointer',
                  height: 30,
                  width: 35,
                  margin: 1,
                  marginRight: { xs: 1 },
                }}
              >
                {/* <AdbIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} /> */}
                <a href="#">
                  <img
                    src="https://staging.tmra.io/static/icons/ic_flag_en.svg"
                    alt="a"
                  />
                </a>
              </Box>
              <Box sx={{ marginRight: { xs: 1, sm: 1 } }}>
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleOpenNavMenu}
                  color="inherit"
                  sx={{
                    display: { xs: 'block', sm: 'block', md: 'none' },
                  }}
                >
                  <MenuIcon />
                </IconButton>
              </Box>
            </Box>

            <Menu
              sx={{ mt: '45px', justifyContent: 'right' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {settings.map((setting) => (
                <MenuItem key={setting} onClick={handleCloseUserMenu}>
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
        </StyledToolbar>
      </Container>
    </AppBar>
  );
};
export default ResponsiveAppBar;
