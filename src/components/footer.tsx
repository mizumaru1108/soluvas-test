import { Box, Link, Typography } from '@mui/material';
import * as React from 'react';

const GetStarted = [
  {
    Name: 'StartFree',
    Link: '#',
  },
  { Name: 'Pricing', Link: '#' },
  { Name: 'How It Work', Link: '#' },
  { Name: 'Customers', Link: '#' },
  { Name: 'Zakat Calculator', Link: '#' },
];
const NonProfit = [
  { Name: 'About Us', Link: '#' },
  { Name: 'Contact Us', Link: '#' },
  { Name: 'Demo Booking', Link: '#' },
  { Name: 'Why Us?', Link: '#' },
  { Name: 'Career', Link: '#' },
];
const Support = [
  { Name: 'FAQ', Link: '#' },
  { Name: 'News', Link: '#' },
  { Name: 'Blog', Link: '#' },
  { Name: 'Help Center', Link: '#' },
  { Name: 'Term of Service', Link: '#' },
  { Name: 'Privacy Policy', Link: '#' },
];

const Footer = () => {
  return (
    <>
      <Box
        sx={{
          background: '#00AB55',

          width: '100%',
          height: { md: '50vh', sm: '65vh' },
          paddingTop: '5vh',
        }}
      >
        <Box
          sx={{
            width: '94%',
            height: { xs: '140vh', sm: '50vh', md: '45vh' },
            display: 'flex',
            flexDirection: { xs: 'column', sm: 'column', md: 'row' },
            marginX: 'auto',
            marginTop: 4,
            marginLeft: { md: 10, xs: 4, sm: 0 },
          }}
        >
          {/* // DIV logo */}
          <Box
            sx={{
              width: '30%',
              backgroundPosition: 'center',
              margin: { xs: 0, sm: 0, md: 0 },
              marginX: { sm: 'auto' },
              marginTop: { sm: -4, md: 0 },
            }}
          >
            <Box
              sx={{
                height: '95%',
                width: '90%',
              }}
            >
              <Box
                sx={{
                  width: '97%',
                  height: '20%',
                }}
              >
                <Box
                  sx={{
                    width: { md: '43%', xs: '100%', sm: '75%' },
                    marginBottom: { sm: 3, xs: 2 },
                    marginLeft: { xs: '17vh', sm: '7vh' },
                  }}
                >
                  <a href="#">
                    <img
                      src="https://staging.tmra.io/static/tmra-brand-light-en.svg"
                      alt="a"
                    />
                  </a>
                </Box>
              </Box>
              <Box
                sx={{
                  width: { xs: '40vh' },
                  height: '30%',

                  marginTop: 0.5,
                }}
              >
                <Typography
                  sx={{
                    textAlign: { xs: 'center', md: 'left' },
                    fontFamily: 'Almarai, sans-serif',
                    textTransform: 'capitalize',
                    fontSize: '2vh',
                    marginLeft: { md: '0vh', sm: '0vh', xs: '10vh' },
                  }}
                >
                  A donation-based crowdfunding platform for muslims
                </Typography>
              </Box>
            </Box>
          </Box>
          {/* // div GetStartedDKK */}
          <Box
            sx={{
              width: '68%',
              display: 'flex',
              flexDirection: { xs: 'column', sm: 'row', md: 'row' },
              margin: 'auto',
              padding: 0,
            }}
          >
            {/* //Div GetStarted */}
            <Box
              sx={{
                height: '95%',
                width: { md: '43%', xs: '100%', sm: '75%' },
                margin: { md: 0, sm: 0 },
                marginTop: { xs: 1, md: 0, sm: 0 },
              }}
            >
              <Box
                sx={{
                  width: '97%',
                  height: '10%',
                  marginBottom: { sm: 1 },
                }}
              >
                <Typography
                  sx={{
                    textAlign: { xs: 'center', md: 'left' },
                    fontFamily: 'Almarai, sans-serif',
                    textTransform: 'capitalize',
                    fontSize: '2vh',
                    fontWeight: 'bold',
                    color: '##212b36',
                  }}
                >
                  GET STAERTED
                </Typography>
              </Box>
              <Box>
                {GetStarted.map((item) => (
                  <Link
                    href={item.Link}
                    sx={{ textDecoration: 'none', color: 'black' }}
                  >
                    <Typography
                      sx={{
                        align: 'left',
                        marginLeft: 0,
                        paddingLeft: 0,
                        paddingTop: 2,
                        fontSize: '1.9vh',
                      }}
                    >
                      {item.Name}
                    </Typography>
                  </Link>
                ))}
              </Box>
            </Box>
            {/* //Div NonProfit */}
            <Box
              sx={{
                height: '95%',
                width: { md: '43%', xs: '100%', sm: '75%' },
                margin: { md: 0, sm: 0 },
                marginTop: { xs: 3, md: 0, sm: 0 },
              }}
            >
              <Box
                sx={{
                  width: '100%',
                  height: '10%',
                  marginBottom: { sm: 1 },
                }}
              >
                <Typography
                  sx={{
                    textAlign: { xs: 'center', md: 'left' },
                    fontFamily: 'Almarai,sans-serif',
                    textTransform: 'capitalize',
                    fontSize: '1.9vh',
                    fontWeight: 'bold',
                    color: '##212b36',
                  }}
                >
                  NONPROFIT FUNDRAISING PLATFORM
                </Typography>
              </Box>
              <Box>
                {NonProfit.map((item) => (
                  <Link
                    href={item.Link}
                    sx={{ textDecoration: 'none', color: 'black' }}
                  >
                    <Typography
                      sx={{
                        align: 'left',
                        marginLeft: 0,
                        paddingLeft: 0,
                        paddingTop: 2,
                        fontSize: '1.9vh',
                      }}
                    >
                      {item.Name}
                    </Typography>
                  </Link>
                ))}
              </Box>
            </Box>
            {/* // Div Support */}
            <Box
              sx={{
                height: '95%',
                width: { md: '43%', xs: '100%', sm: '75%' },
                margin: { md: 0, sm: 0 },
                marginTop: { xs: 3, md: 0, sm: 0 },
              }}
            >
              <Box
                sx={{
                  width: '97%',
                  height: '10%',
                  marginBottom: { sm: 1 },
                }}
              >
                <Typography
                  sx={{
                    textAlign: { xs: 'center', md: 'left' },
                    fontFamily: 'Almarai,sans-serif',
                    textTransform: 'capitalize',
                    fontWeight: 'bold',
                    fontSize: '2vh',
                    color: '##212b36',
                  }}
                >
                  SUPPORT
                </Typography>
              </Box>
              <Box>
                {Support.map((item) => (
                  <Link
                    href={item.Link}
                    sx={{ textDecoration: 'none', color: 'black' }}
                  >
                    <Typography
                      sx={{
                        align: 'left',
                        marginLeft: 0,
                        paddingLeft: 0,
                        paddingTop: 2,
                        fontSize: '1.9vh',
                      }}
                    >
                      {item.Name}
                    </Typography>
                  </Link>
                ))}
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default Footer;
